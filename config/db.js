const mongoose = require("mongoose");
const User = require("../models/users");
const { comparePasswords, generateHash } = require("../services/encrypt-decrypt");
const { encodeUser } = require("../services/jwt");
class Controller {
  constructor() {
    this.connect();
  }
  async connect() {
    try {
      await mongoose.connect(process.env.DB_HOST, {
        useNewUrlParser: true,
        useUnifiedTopology: true
      })
      console.log("conectado a la BD")
    } catch (e) {
      console.error(e)
    }
  }
  async login(email, password) {
    try {
      const user = await User.findOne({ email: email, inactive: false }).exec()
      if (await comparePasswords(user.password, password)) {
        return encodeUser(user)
      } else {
        return false;
      }
    } catch (exception) {
      throw exception
    }
  }
  async register(firstname, lastname, email, password) {
    try {
      const hashedPassword = await generateHash(password);
      const userAttr = {
        first_name: firstname,
        last_name: lastname,
        email,
        password: hashedPassword,
        created_at: new Date(),
        inactive: 0
      }
      User.create({ ...userAttr })
    } catch (exception) {
      throw exception
    }

  }
}

exports.controller = new Controller()