const { Schema, model } = require('mongoose');

const UserSchema = new Schema({
    first_name: {
        type: String,
        required: true
    },
    last_name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    created_at: {
        type: Date,
        required: true
    },
    inactive: {
        type: Boolean,
        required: false,
        default: false,
    }
});

module.exports = model('User', UserSchema);