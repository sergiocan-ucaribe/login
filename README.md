#### Descripción general
Esta api registra usuarios en mongo
Y devuelve un token cuando alguien inicia sesión con las credenciales correctas.
Las ramas de desarrollo y master fueron protegidas por Sergio para evitar commit o merge sin revisiones previas.
Las base de datos en mongo fue instanciada
por Mijares.


#### Correr el proyecto

* npm install
* npm start
* On insomnia or postman



#### register: create a POST petition to: localhost:3000/user/register with the following data:

* firstname
* lastname
* email
* password
#### login: create a POST petition to: localhost:3000/user/login with the following data:

* email
* password


