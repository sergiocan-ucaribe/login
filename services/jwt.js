const { sign, verify } = require('jsonwebtoken');
const secret = process.env.SECRET;

const encodeUser = (User) => {
  try {
    const encoded = sign({ User }, secret);
    return encoded;
  } catch (exception) {
    throw exception;
  }
}
const decodeToken = (token) => {
  try {
    const decoded = verify(token, secret);
    return decoded;
  } catch (exception) {
    throw exception;
  }
}

module.exports = {
  encodeUser,
  decodeToken
};