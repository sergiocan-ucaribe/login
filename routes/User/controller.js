const { controller: UserController } = require('../../config/db');
module.exports = {
  register: async (req, res) => {
    try {
      const { firstname, lastname, email, password } = req.body;
      if (firstname && lastname && email && password) {
        await UserController.register(firstname, lastname, email, password);
        res.status(200).send({ msg: "User registered" });
      } else {
        res.status(400).send({ msg: "Not enought data for account" });
      }
    } catch (exception) {
      throw exception
    }
  },
  login: async (req, res) => {
    try {

      const { email, password } = req.body;
      const token = await UserController.login(email, password);
      if (token) {
        res.status(200).send({
          msg: "User logged",
          token
        });
      } else {
        res.status(401).send({ msg: "User not logged" });
      }
    } catch (exception) {
      throw exception
    }

  },
}